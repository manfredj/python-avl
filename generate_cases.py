# Imports:

import re
import os

# Variables:

AR = 6.5  # Aspect Ratio
width = 1.0 # Wing Span (also b)
chord_average = width / AR # At center of wing

# Generated Wing Twist:
lower_twist = -4
upper_twist = 1
step_twist = 1

# Generated Taper Ratio:
lower_taper = 0.6
upper_taper = 0.75
step_taper = 0.05

# Open the base file for editing:
base_file = open("base_file.txt")
base_lines = base_file.readlines()

# Delete old generated files in input_files:
filelist = ["input_cases/" + f for f in os.listdir("input_cases") if f.endswith(".avl")]
for f in filelist:
    os.remove(f)

# Function to generate AVL Input File:
def generate(count, taper, twist):
    filename = "input_cases/" + str(count) + "_" + str(taper) + "_" + str(twist) + ".avl"

    file = open(filename, mode='w')

    # Copy 'base' template:
    lines = base_lines[:]

    # Count:
    lines[:] = [re.sub(r"\[count\]", str(count), line) for line in lines]

    # Twist:
    lines[:] = [re.sub(r"\[twist\]", str(twist), line) for line in lines]

    # Taper
    lines[:] = [re.sub(r"\[taper\]", str(taper), line) for line in lines]

    # Calculate chords, then round for AVL:
    chord_root = chord_average * (2.0 / (1 + taper))
    chord_tip = chord_average * (2.0 / (1 + taper ** -1))

    chord_root = round(chord_root, 2)
    chord_tip = round(chord_tip, 2)


    tip_le = chord_root / 2.0 - chord_tip / 2.0

    tip_le = round(tip_le, 2)

    # Tip Leading Edge Position
    lines[:] = [re.sub(r"\[tip_le\]", str(tip_le), line) for line in lines]


    # Chord Root
    lines[:] = [re.sub(r"\[chord_root\]", str(chord_root), line) for line in lines]

    # Chord Tip
    lines[:] = [re.sub(r"\[chord_tip\]", str(chord_tip), line) for line in lines]


    # Save output:
    file.writelines(lines)
    print(lines)
    file.close()


limit_taper = lower_taper

count = 100

while limit_taper <= upper_taper:

    limit_twist = lower_twist

    while limit_twist <= upper_twist:
        generate(count, limit_taper, limit_twist)
        count = count + 1
        limit_twist = limit_twist + step_twist
        limit_twist = round(limit_twist, 3)

    limit_taper = limit_taper + step_taper
    limit_taper = round(limit_taper, 3)
