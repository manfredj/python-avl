import re
import os
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm

# range_taper = 0.6
# range_twist = 5

data_array = []
# plot[0] = #np.array()

# for i in range(0, 4):
#     plot.append(np.zeros((range_taper, range_twist)))

# print(plot)

list = []

for f in os.listdir("output_cases"):
    if not f.startswith('.'):
        list.append(f)

print(list)

# Pattern:

for file in list:
    content = open("output_cases/" + file, 'r').read().split()

    # print(content)

    content = ' '.join(content)

    # Process Filename for data:
    name = re.match(r"^([0-9]+)_([\.0-9]+)_([-\.0-9]+)\.avl\.txt", file)
    count = name.group(1)
    taper = name.group(2)
    twist = name.group(3)

    # Skip if empty file
    if not content:
        continue

    # Skip if file contains invalid data (Not a Number)
    if re.search(r".+(NaN).+", content):
        data_array.append([int(count), float(taper), float(twist), float('nan'), 0, float('nan')])
        continue


    # (CDtot\s=\s+)([0-9].[a-zA-Z0-9]+)


    # Total Drag
    drag_total = re.search(r".+CDtot\s=\s+([0-9].[a-zA-Z0-9]+)+", content).group(1)
    drag_total = float(drag_total)

    # Total Lift
    lift_total = re.search(r".+CLtot\s=\s+([0-9].[a-zA-Z0-9]+)+", content).group(1)
    lift_total = float(lift_total)

    # Print Nice Data:
    print("Case: " + count, "Taper: " + taper, "Twist: " + twist)
    print("Total CL: ", lift_total)
    print("Total CD: ", drag_total)
    print("Lift over Drag: ", lift_total / drag_total)
    print("\n")

    # Insert into data arrays:

    # x = np.float64(range_taper) / np.float64(taper)
    # y = np.float64(range_twist) / np.float64(twist)
    # if np.isinf(x): x = 0
    # if np.isinf(y): y = 0
    # x = int(x)
    # y = int(y)
    # data_array = np.append(data_array, [[count, taper, twist, lift_total, drag_total]], axis=0)
    data_array.append([int(count), float(taper), float(twist), lift_total, drag_total, lift_total / drag_total])
    # print("Position: ", x, y)
    # #    x = np.array([lift_total])
    #
    # plot[0][x, y] = lift_total

nrows, ncols = 4, 6

# F = plt.figure(1, (5.5, 3.5))
# grid = ImageGrid(F, 111,  # similar to subplot(111)
#                  nrows_ncols=(nrows, ncols),
#                  axes_pad=0.1,
#                  add_all=True,
#                  label_mode="L",
#                  )

data_array = np.asarray(data_array)
# print(data_array)

grid = data_array[:, 5].reshape((nrows, ncols))

grid = np.flipud(grid)

x = data_array[:, 2]
y = data_array[:, 1]

plt.xlabel('Wing Twist (degrees)')
plt.ylabel('Taper Ratio (-)')
plt.title('CL/CD Graph')


# print(grid.argmax(axis=0))

# i,j = np.unravel_index(grid.argmax(), grid.shape)

# print(i, j)

# max = np.where(grid == grid.max(), )
# max = np.where(grid == grid.max())
#
# print(grid)
#
# indices = np.where(grid==grid.argmax())
# print(indices)

# max = np.nanargmax(grid, axis=1)
# print(max)

# exit()

# i, j = max[0], max[1]
# print(i,j)
plt.axvline(-3)
plt.axhline(0.7)

plt.axvline(0, color='k')
plt.axhline(0, color='k')

graph = plt.imshow(grid, extent=(-4.5, 1.5, 0.575, 0.775), interpolation='bilinear', cmap=cm.RdBu, aspect='auto')

# print(grid)

plt.axis([-4.5, 1.5, 0.575, 0.775])
cb = plt.colorbar(graph)
cb.set_label("CL/CD")
cb.set_ticks([round(np.min(grid) + 0.015, 2), 31.8, 32.0, round(np.max(grid) - 0.015, 2)])

plt.draw()
plt.show()
