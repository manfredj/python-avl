# !/usr/bin/env python3
import os
import time
from subprocess import Popen, PIPE, DEVNULL
import re

root = os.getcwd()
list = []

for f in os.listdir("input_cases"):
    if not f.startswith('.'):
        list.append(f)

print("Full list:\n", list)
print(os.path.abspath("avl"))

for filename in list:

    # start AVL Program
    x = int(filename[:1])
    print(x)
    with Popen(os.path.abspath("avl"), stdin=PIPE, stdout=PIPE, bufsize=1,
               universal_newlines=True) as process:
        time.sleep(1)

        print("Loaded " + filename)
        # start program with LOAD and filename:
        print("LOAD " + os.path.abspath(r"input_cases/" + filename), file=process.stdin)
        time.sleep(2)

        # Operate Mode and CL=0.6358:
        time.sleep(1)
        print("OPER", file=process.stdin)
        print("A", file=process.stdin)
        print("C", file=process.stdin)
        print("0.6358", file=process.stdin)

        # eXecture the calculations:
        time.sleep(1)
        print("X", file=process.stdin)

        # Extract Bash Data:
        out, err = process.communicate()

        # Save data to the output_cases folder
        print(err)
        file = open("output_cases/" + filename + ".txt", 'w')
        file.writelines(out)
        print(out)

        # If there was an error (Not a Number), don't delete the input file.
        if re.search(r"(NaN)", out):
            continue
        else:
            os.remove("input_cases/"+filename)
            print("Finish!")
